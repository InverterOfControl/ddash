using System.Collections.Generic;
using System.Threading.Tasks;
using YouTrackSharp.Issues;
using YouTrackSharp.Projects;

namespace Ddash.Services.Interfaces.TileServices {
    public interface IYouTrackService {

        Task<ICollection<Project>> GetProjectsAsync();

        Task<ICollection<Issue>> GetIssuesForProjectAsync(string projectId);
    }
}