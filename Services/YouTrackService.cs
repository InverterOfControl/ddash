using System.Collections.Generic;
using Ddash.Services.Interfaces;
using YouTrackSharp;
using YouTrackSharp.Projects;
using System.Threading.Tasks;
using System.Linq;
using YouTrackSharp.Issues;
using Microsoft.Extensions.Options;
using Ddash.Settings;
using Ddash.Services.Interfaces.TileServices;
using Microsoft.AspNetCore.Mvc;

namespace Ddash.Services
{
    public class YouTrackService : IYouTrackService, ITileService
    {
        public UsernamePasswordConnection Connection { get; private set; }

        public string Name => "YouTrack";

        private readonly AppSettings _settings;

        public YouTrackService(IOptions<AppSettings> settings){
            this._settings = settings.Value;
            this.Connection = new UsernamePasswordConnection(
                this._settings.ExternalServices.YouTrackSettings.URL,
                this._settings.ExternalServices.YouTrackSettings.User,
                this._settings.ExternalServices.YouTrackSettings.Password
                );
        }

        public async Task<ICollection<Project>> GetProjectsAsync()
        {
            var projectsService = this.Connection.CreateProjectsService();

            return await projectsService.GetAccessibleProjects();
        }

        public async Task<ICollection<Issue>> GetIssuesForProjectAsync(string projectId)
        {
            var issuesService = this.Connection.CreateIssuesService();

            return await issuesService.GetIssuesInProject(projectId);
        }

        public JsonResult Render()
        {
            throw new System.NotImplementedException();
        }
    }
}