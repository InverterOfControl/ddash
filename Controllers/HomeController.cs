﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ddash.Models;
using Ddash.Services.Interfaces;
using Ddash.Services.Interfaces.TileServices;
using Microsoft.Extensions.Logging;

namespace Ddash.Controllers
{
    public class HomeController : Controller
    {
        private readonly IYouTrackService _youTrackService;
        private readonly ILogger<HomeController> _logger;

        public HomeController(IYouTrackService youTrackService, ILogger<HomeController> logger){
            this._youTrackService = youTrackService;
            this._logger = logger;
        }

        public IActionResult Index()
        {
            this._logger.LogInformation("Index was called!");
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
