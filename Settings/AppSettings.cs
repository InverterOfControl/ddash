﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ddash.Settings
{
    public class AppSettings {
        public ExternalServices ExternalServices { get; set; }

    }

    public class ExternalServices {
        public YouTrackSettings YouTrackSettings { get; set; }
    }

    public class YouTrackSettings {
        public string URL { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }
}
